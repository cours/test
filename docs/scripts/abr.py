# --- PYODIDE:env --- #

import js


class ArbreBinaire:
    def __init__(self, valeur):
        self.set_valeur(valeur)
        self.filsG = None
        self.filsD = None

    def insert_gauche(self, valeur):
        if self.filsG == None:
            self.filsG = ArbreBinaire(valeur)
        else:
            nouveau_noeud = ArbreBinaire(valeur)
            nouveau_noeud.filsG = self.filsG
            self.filsG = nouveau_noeud

    def insert_droit(self, valeur):
        if self.filsD == None:
            self.filsD = ArbreBinaire(valeur)
        else:
            nouveau_noeud = ArbreBinaire(valeur)
            nouveau_noeud.filsD = self.filsD
            self.filsD = nouveau_noeud

    def get_valeur(self):
        return self.valeur
    
    def set_valeur(self,nouvelle_valeur):
        self.valeur = nouvelle_valeur

    def get_gauche(self):
        return self.filsG

    def get_droit(self):
        return self.filsD  

def hauteur(arbre) :
    if arbre is not None :
        return 1 + max (hauteur(arbre.get_gauche()), hauteur(arbre.get_droit()))
    else : 
        return 0

def taille(arbre) :
    if arbre is not None :
        return 1 + taille(arbre.get_gauche()) + taille(arbre.get_droit())
    else : 
        return 0

def nom(arbre):
    if arbre != None:
        return (arbre.get_valeur(),nom(arbre.get_gauche()),nom(arbre.get_droit()))

def representation(noeud, nom_noeud, aretes):
            if noeud is not None:
                nom_noeud.append(str(noeud.valeur))
                # Appel récursif de la fonction representation
                if noeud.filsG is not None:
                    representation(noeud.filsG, nom_noeud, aretes)
                    aretes.append((str(noeud.valeur) , str(noeud.filsG.valeur)))
                if noeud.filsD is not None:
                    representation(noeud.filsD, nom_noeud, aretes)
                    aretes.append((str(noeud.valeur) , str(noeud.filsD.valeur)))



def vers_abr(arbre):
    aretes = []
    nom_noeud = []
    representation(arbre, nom_noeud, aretes)
    graphique=f"graph TB \n"
    for i in range(len(nom_noeud)):
        graphique = graphique + f"{nom_noeud[i]}(({nom_noeud[i]})) \n"
    for i in range(len(aretes)):
        graphique = graphique + f"{aretes[i][0]} --- {aretes[i][1]} \n"
    return graphique

def dessine(arbre):
    elt_id = "fig2"
    elt = js.document.getElementById(elt_id)
    elt.setAttribute("class", "mermaid")
    elt.removeAttribute("data-processed")
    elt.innerHTML = vers_abr(arbre)
    js.mermaid.run()


# --- PYODIDE:code --- #

arbre = ArbreBinaire(15)
arbre.insert_gauche(6)
arbre.insert_droit(18)
noeud_6 = arbre.get_gauche()
noeud_6.insert_gauche(3)
noeud_6.insert_droit(7)

dessine(arbre)