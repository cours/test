# --- PYODIDE:env --- #

    # La section env est exécutée avant le code utilisateur.
    # Son contenu n'est pas visible de l'utilisateur mais tout ce qui y est défini
    # est ensuite disponible dans l'environnement.
    #
    # Si le code de la section ENV lève une erreur, rien d'autre ne sera exécuté.
from math import *


# --- PYODIDE:code --- #

def pythagore(...):
    ...


# --- PYODIDE:corr --- #

def pythagore(a, b):
    c = sqrt(a**2 + b**2)
    return c


# --- PYODIDE:tests --- #

assert pythagore(3, 4) == 5


# --- PYODIDE:secrets --- #

    # Contient les tests privés. Ces tests ne sont pas visibles par l'utilisateur.
    #
    # ATTENTION: il est impératif d'utiliser des messages dans les assertions des
    # tests privés, sinon l'utilisateur ne peut pas déboguer son code car `print`
    # est désactivé durant ces tests !
    # À vous de choisir le niveau d'information que vous voulez fournir dans le
    # message.
    #
    # Par ailleurs, il est conseillé d'utiliser une fonction pour éviter que des
    # variables des tests ne se retrouvent dans l'environnement global.

def tests():

        assert pythagore(4, 5) == sqrt(41)

tests()         # Ne pas oublier d'appeler la fonction de tests... ! x)
del tests       # Si vous ne voulez pas laisser de traces...


# --- PYODIDE:post --- #

    # La section post contient du code de "nettoyage", à appliquer systématiquement
    # après que le code et les tests aient été lancés.
    # Ce contenu est exécuté même si une erreur a été levée précédemment, SAUF si
    # cette erreur provient de la section ENV.