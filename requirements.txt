pyodide-mkdocs-theme
mkdocs-awesome-pages-plugin
selenium
mkdocs-exclude-search
mkdocs-video
mkdocs-enumerate-headings-plugin
git+https://github.com/Epithumia/mkdocs-sqlite-console.git
mkdocs-add-number-plugin