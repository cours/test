## Documentation du thème

[Ici](https://frederic-zinelli.gitlab.io/pyodide-mkdocs-theme/)

## Terminal Python

{{ terminal(TERM_H=6) }}

## IDE

{{ IDE('scripts/exo', ID=1, MAX=1, TERM_H=10) }}

## IDE vertical

{{ IDEv('scripts/exo', MAX_SIZE=10, ID=2) }}

## QCM

{{multi_qcm(
    [
        """
        On a saisi le code suivant :
        ```python title=''
        n = 8
        while n > 1:
            n = n/2
        ```

        Que vaut `n` après l'exécution du code ?
        """,
                [
                    "2.0",
                    "4.0",
                    "1.0",
                    "0.5",
                ],
                [3]
    ],
    

    [
        "Quelle est la machine qui va exécuter un programme JavaScript inclus dans une page HTML ?",
        [
            "La machine de l’utilisateur sur laquelle s’exécute le navigateur web.",
            "La machine de l’utilisateur ou du serveur, selon celle qui est la plus disponible.",
            "La machine de l’utilisateur ou du serveur, suivant la conﬁdentialité des données manipulées.",
            "Le serveur web sur lequel est stockée la page HTML."
        ],
        [1],
    ],


    [
        """
    Quelle expression permet d'accéder au numéro de Tournesol :
    ```python title=''
    repertoire = [{'nom': 'Dupont', 'tel': 5234}, {'nom': 'Tournesol', 'tel': 5248}, {'nom': 'Dupond', 'tel': 5237}]
    ```
        """,
        [
            "`#!py repertoire[1]['tel']`",
            "`#!py repertoire['tel'][1]`",
            "`#!py repertoire['Tournesol']`",
            "`#!py repertoire['Tournesol']['tel']`",
        ],
        [1],
        {'multi':True}
    ],


    multi = False,
    qcm_title = "Un QCM avec mélange automatique des questions (bouton en bas pour recommencer)",
    ID = 0,
    DEBUG = False,
    shuffle = True,
)}}

## Mermaid et IDE

### Exemple 1

{{IDE('scripts/mermaid1', MERMAID=True)}}

{{figure(admo_title="Ma figure", inner_text="")}}

### Exemple 2

Cliquer sur le bouton ci-dessous pour exécuter du code très caché!

{{ py_btn('scripts/mermaid2', MERMAID=True) }}

{{ figure('btn-fig-id', admo_title="Ma figure", inner_text="") }}

## Mermaid avec Python


### premier essai

<!---Il faut absolument au moins un graphe vide de Mermaid pour que ça fonctionne dans un IDE--->
```mermaid
    graph
```

{{ IDE('scripts/graph') }}


<pre id="fig" data-processed="true"></pre>

### arbre binaire

{{ IDE('scripts/abr') }}


<pre id="fig2"></pre>


## Turtle dans pyodide

{{ IDE('scripts/turtle', TERM_H=7) }}

{{ figure('cible_1') }}

## P5

{{ IDE('scripts/exo_P5') }}

{{ figure("figureP5", admo_title="Ma figure P5", inner_text="du texte à l'intérieur de l'admonition") }}