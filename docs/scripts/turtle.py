# --------- PYODIDE:env --------- #
from js import document
if "restart" in globals():
    restart()

def m_a_j(cible):
    done()
    document.getElementById(cible).innerHTML = Screen().html

_cible = 'cible_1'

# --------- PYODIDE:code --------- #
from turtle import *  # pour utiliser la tortue
setup(640, 480)  # pour définir la taille de la fenêtre

forward(50)  # avancer de 50 pixels
left(60)     # tourner à gauche de 60 degrés
forward(50)  # avancer de 50 pixels
right(80)    # tourner à droite de 80 degrés
forward(100) # avancer de 100 pixels

# --------- PYODIDE:post --------- #
if Screen().html is None:
    forward(0)
m_a_j(_cible)
# --------- PYODIDE:post_term --------- #
if "m_a_j" in globals():
    m_a_j(_cible)