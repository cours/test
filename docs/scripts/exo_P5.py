import p5

def setup():
    p5.createCanvas(200,200)
    p5.background(50)

def draw():
    p5.circle(p5.mouseX, p5.mouseY, 50)

p5.run(setup, draw, target="figureP5")