import js

def vers_graph():
    return f"""
    graph TB
    N((N))
    N0((N0))
    N --- N0
    N1((N1))
    N --- N1
    """


def dessine():
    elt_id = "fig"
    elt = js.document.getElementById(elt_id)
    elt.setAttribute("class", "mermaid")
    elt.removeAttribute("data-processed")
    elt.innerHTML = vers_graph()
    js.mermaid.run()

dessine()